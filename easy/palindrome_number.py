# Determine whether an integer is a palindrome. An integer is a palindrome when it reads the same backward as forward.
#
# Example 1:
#
# Input: 121
# Output: true
# Example 2:
#
# Input: -121
# Output: false
# Explanation: From left to right, it reads -121. From right to left, it becomes 121-. Therefore it is not a palindrome.
# Example 3:
#
# Input: 10
# Output: false
# Explanation: Reads 01 from right to left. Therefore it is not a palindrome.
#
#
# class Solution:
#     def isPalindrome(self, x):
#         """
#         :type x: int
#         :rtype: bool
#         """

class Solution:
    def isPalindrome(self,x):
        if x < 0:
            return False
        else:
            original_number = x
            reversed_number = 0
            while x > 0:
                rem = x % 10
                reversed_number = reversed_number * 10 + rem
                x = x // 10
            if reversed_number == original_number:
                return True
            else:
                return False

if __name__ == '__main__':
    number = int(input("Enter a number: "))
    s = Solution()
    print(s.isPalindrome(number))