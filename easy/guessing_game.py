import random


guesses = []
picked_integer = random.randint(1, 100)

print("Let's play a guessing game, shall we?\n")
print("Rules:")
print("I think of a number.")
print("1. If your guess is less than 1 or greater than 100, I say 'OUT OF BOUNDS'")
print("2. On your first turn, if your guess is within 10 of the number, I say 'WARM!',")
print("farther than 10 away from the number, I say 'COLD!'")
print("3. On all subsequent turns, if your guess is closer to the number than the previous guess, I say 'WARMER!',")
print("farther from the number than the previous guess, I say 'COLDER!'")
print("4. When you guess correctly, you win!\n")
print("Let's start!\n")

while True:
    guess = int(input("Guess a number: "))

    if guess not in range(1, 101):
        print("OUT OF BOUNDS. Please guess between 1 to 100")

    else:
        guesses.append(guess)
        if len(guesses) < 2:
            if guesses[-1] == picked_integer:
                print("CORRECT ANSWER")
                print(f'you guessed in {len(guesses)} chances')
                break
            elif abs(guesses[-1] - picked_integer) > 10:
                print("COLD")
            else:
                print("WARM")
        else:
            if guesses[-1] == picked_integer:
                print("CORRECT ANSWER")
                print(f'you guessed in {len(guesses)} chances')
                break
            elif abs(guesses[-1] - picked_integer) < abs(guesses[-2] - picked_integer):
                print("WARMER")
            else:
                print("COLDER")
