# Given an array of integers, return indices of the two numbers such that they add up to a specific target.
#
# You may assume that each input would have exactly one solution, and you may not use the same element twice.
#
# Example:
#
# Given nums = [2, 7, 11, 15], target = 9,
#
# Because nums[0] + nums[1] = 2 + 7 = 9,
# return [0, 1]
#
# --------------------------------------
#
# class Solution:
#     def twoSum(self, nums, target):
#         """
#         :type nums: List[int]
#         :type target: int
#         :rtype: List[int]
#         """


class Solution:
    def twoSum(self, nums, target):
        for index in range(len(nums)):
            diff = target - nums[index]
            if diff in nums and nums.index(diff) != index:
                return[index, nums.index(diff)]



if __name__ == '__main__':
    s = Solution()
    num_array = list()
    n = int(input("Enter no of elements: "))
    if n > 1:
        for i in range(n):
            num_array.append(int(input("Enter num {}: ".format(i+1))))
        target = int(input("Enter target: "))
        print(s.twoSum(num_array, target))
    else:
        print("I need at least 2 elements!")



