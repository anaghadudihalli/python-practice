# -*- coding: utf-8 -*-
#
#
# Given a 32-bit signed integer, reverse digits of an integer.
#
# Example 1:
# Input: 123
# Output: 321
#
# Example 2:
# Input: -123
# Output: -321
#
# Example 3:
# Input: 120
# Output: 21
#
# Note:
# Assume we are dealing with an environment which could only store integers within the 32-bit signed integer
# range: [−231,  231 − 1]. For the purpose of this problem, assume that your function returns 0 when the
# reversed integer overflows.
#
# --------------------------------
#
# class Solution:
#     def reverse(self, x):
#         """
#         :type x: int
#         :rtype: int
#         """


class Solution:
    def reverse(self, x):
        reversed_num = 0
        negative = False
        if x < 0:
            negative = True
            x = abs(x)

        while int(x) > 0:
            rem = x % 10
            reversed_num = reversed_num * 10 + rem
            x = x // 10

        if abs(reversed_num) > 2**31 - 1:
            return 0
        else:
            if negative is True:
                return -reversed_num
            else:
                return reversed_num


if __name__ == '__main__':
    s = Solution()
    print("Enter a number to reverse: ")
    original = int(input())
    r = s.reverse(original)
    if r == 0:
        print("uh oh, integer overflow. I take only 32 bit signed integer")
    else:
        print("Reversed number is ", r)





